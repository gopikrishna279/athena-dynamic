import {
  Component,
  Inject,
  OnInit
} from '@angular/core';
import { baWizzardComponent } from '../../shared/baWizzard/baWizzard.component';
import { baWizzardStepComponent } from '../../shared/baWizzard/baWizzardStep.component';
import * as _ from 'lodash';

import { ThreadsService } from './../thread/threads.service';
import { MessagesService } from './../message/messages.service';

import { Thread } from './../thread/thread.model';
import { Message } from './../message/message.model';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { SharedService } from '../shared.service';

import { UserService } from '../../shared/userservice';

@Component({
  selector: 'claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.css']
})
export class ClaimComponent implements OnInit{

  public form:FormGroup;
  public cid:AbstractControl;
  public fname:AbstractControl;
  public lname:AbstractControl;
  public pcode:AbstractControl;
  public illness:AbstractControl;
  public age:AbstractControl;
  public rfv:AbstractControl;
  public doi:AbstractControl;
  public healthCareClaimType: AbstractControl;
  public claimFormat: AbstractControl;
  public status: AbstractControl;
  public created: AbstractControl;
  public emer:AbstractControl;
  public dfc:AbstractControl;
  public dos:AbstractControl;
  public tos:AbstractControl;
  public pos:AbstractControl;
  public pci:AbstractControl;
  public dc:AbstractControl;
  public validationbtn:boolean=false;
  diagnosis: number;
  timer;
  procedurecodeapi;
  diagnosiscodeapi;
  procedure: number;
  private _isCompleted: boolean = false;
  private iswarning: boolean = false;
  private iswarning1: boolean = false;
  public procedurecode:boolean=false;
  public healthCareBackground: boolean=false;
  public statusBackground: boolean=false;
  public rulesEngineMessage: string;
  public cellbackground: boolean = false;
  public warning;
  public warning1;
  public successFlag: boolean= false;
  public localHealthStore:String;
  data;
  public typeofservice;
  typeservice;
  placeserice;
  memid: number;
  firstname;
  public placeofservice;
  public service:boolean = false;
  public reason;
  dateofillness;
  dateoffirstconsult;
  similarillness;
  i=0;
  checkbox = [];
  /* details= [];
 data1;
 data2;*/
  emers;
  public success:boolean=false;
  public ocCodesFlag=false;
  emergency;
  constructor(
    public fb: FormBuilder,
    private sharedsevice: SharedService,
    private userservice: UserService
  ) {

    this.form = fb.group({
      'healthCareClaimType': ['', Validators.required],
      'claimFormat': ['', Validators.required],
      'status': ['', Validators.required],
      'created': ['', Validators.required],
      'description': ['', Validators.required],
      'claimRuleCategory': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'dateOfBirth': ['', Validators.required],
      'memberId': ['', Validators.required],
      'claimRuleCategoryId': ['', Validators.required],
      'claimSubmissionCategoryId': ['', Validators.required],
      'insuranceReportingCategoryId': ['', Validators.required],
      'rpfirstName': ['', Validators.required],
      'rplastName': ['', Validators.required],
      'rnpi': ['', Validators.required],
      'rBilledName': ['', Validators.required],
      'rproviderType': ['', Validators.required],
      'rMedicalNpi': ['', Validators.required],
      'spfirstName': ['', Validators.required],
      'splastName': ['', Validators.required],
      'snpi': ['', Validators.required],
      'sBilledName': ['', Validators.required],
      'sproviderType': ['', Validators.required],
      'sMedicalNpi': ['', Validators.required],
      'code1': ['', Validators.required],
      'code2': ['', Validators.required],
      'code3': ['', Validators.required],
      'code4': ['', Validators.required],
      'date1': ['', Validators.required],
      'date2': ['', Validators.required],
      'date3': ['', Validators.required],
      'date4': ['', Validators.required],
      'id1': ['', Validators.required],
      'id2': ['', Validators.required],
      'id3': ['', Validators.required],
      'id4': ['', Validators.required]
    });
    /*this.cid = this.form.controls['cid'];
    this.fname = this.form.controls['fname'];
    this.lname = this.form.controls['lname'];
    this.pcode = this.form.controls['pcode'];
    this.age = this.form.controls['dob'];
    this.rfv = this.form.controls['rfv'];
    this.doi = this.form.controls['doi'];
    this.dfc = this.form.controls['doi'];
    this.illness = this.form.controls['doi'];
    this.dos = this.form.controls['doi'];
    // this.emer = this.form.controls['emer'];
    this.tos = this.form.controls['tos'];
    this.pos = this.form.controls['pos'];
    this.pci = this.form.controls['pci'];
    this.dc = this.form.controls['dc'];
    this.healthCareClaimType=this.form.controls['healthCareClaimType'];*/
  }





  ngOnInit() {

    let that=this;
    var i=1;
    $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='name\"+i+\"' type='date' placeholder='date' class='form-control input-md'   formControlName='dos'  /></td><td><input name='name"+i+"' type='text' placeholder='Type of Service' class='form-control input-md' disabled /> </td><td><input  name='mail"+i+"' type='text' placeholder=' Place of Service'  class='form-control input-md' disabled></td><td><input  name='mobile"+i+"' type='text' placeholder='Procedure Code Identify'  class='form-control input-md'></td></td><td><input name='name\"+i+\"' type='text' placeholder=' Diagnosis Code' class='form-control input-md'  /></td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');


      i++;
    });
    $("#delete_row").click(function(){
      if(i>1){
        $("#addr"+(i-1)).html('');
        i--;
      }
    });
    this.myCallBack();
    this.timer = setInterval(() => {
      this.myCallBack();

    }, 1500);
  }
  validation(value) {
    this.success  = false;  //false
    this.service = false;
    //this.validationbtn = true;
    this.cellbackground = false; //false
    this.iswarning = false;
    this.validationbtn = true; //true
    this.warning1 = value;
    if(this.warning1=='no'){
      this.iswarning1 = true;
      this.sharedsevice.insertData('no');

    }
    else {
      this.iswarning1 = false;
    }


  }


  validation1(value) {
    this.service = false; //false
    this.success  = false;
    this.cellbackground = false;
    this.iswarning1 = false;
    this.validationbtn = true;
    this.warning= value;

    if(this.warning == 'yes'){
      this.iswarning = true;
    }
    else {
      this.iswarning = false;
    }

  }



  validationbuttotn(){
    if ( this.validationbtn) {
      this.cellbackground = false;
    }
  }

  idstore(values) {


  }
  agestore(values) {
    this.age = values.age;
    console.log(this.age );

    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }


  }
  firststore(values){

    this.fname = values.fname;
    console.log(this.fname );
    this.firstname = this.fname;


    if (this.memid  == 5236 && this.firstname !== 'gopi' && typeof this.firstname !== "undefined" ) {
      this.sharedsevice.insertData( "first name does not matched with member id." );
    }

    if(this.memid  == 5236 && this.firstname !== 'Gopi' && typeof this.firstname !== "undefined"){
      this.sharedsevice.insertData( "first name does not matched with member id.");
    }


    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }

    if(this.memid  == 5236 && this.firstname == 'gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "Hey, looks good first name  matched with member id." );
    }

    if(this.memid  == 5236 && this.firstname == 'Gopi' && typeof this.firstname !== "undefined" ){
      this.sharedsevice.insertData( "Hey, looks good first name  matched with member id" );
    }


  }
  laststore(values){
    this.lname = values.lname;
    console.log(this.lname );


    if(this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi') && typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }

    console.log(this.memid);

    console.log(this.firstname);
    console.log(this.age);
    console.log(this.lname);
    console.log(this.rfv);
  }
  reasonstore(values) {
    this.rfv = values.rfv;
    console.log(this.rfv );
    this.reason = this.rfv;

    if (this.memid == 5236 && typeof this.memid !== "undefined" && (this.firstname == 'Gopi' || this.firstname == 'gopi')&& typeof this.age !== "undefined" && typeof this.firstname !== "undefined" && typeof this.lname !== "undefined" && typeof this.reason !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated first step." );
    }



  }


  //second step validation
  secondstep(){
    if(typeof this.dateofillness !== "undefined" && typeof this.emergency !== "undefined" && typeof  this.dateoffirstconsult !== "undefined" && typeof this.similarillness !== "undefined"){
      this.sharedsevice.insertData( "Hey, you compleated second step. you just one step away complete.");
    }
  }

  doistore(values) {
    this.doi = values.doi;
    console.log(this.doi);

    this.dateofillness = this.doi;
    this.secondstep();

  }

  claimControlStore(values) {
    console.log("In claim store " + values.healthCareClaimType);
    this.localHealthStore = values.healthCareClaimType;
    console.log(" local var : " + this.localHealthStore);
  }
  claimFormatStore(values) {
    console.log(values.claimFormat);
  }
  statusStore(values) {
    console.log(values.status);
  }
  createdStore(values) {
    console.log(values.created);
  }
  descriptionStore(values) {
    console.log(values.description);
  }
  claimRuleCategoryStore(values) {
    console.log(values.claimRuleCategory);
    this.sharedsevice.insertData('Hey you completed first step');
  }
  firstNameStore(values) {
    console.log(values.firstName);
  }
  lastNameStore(values) {
    console.log(values.lastName);
  }
  dateOfBirthStore(values) {
    console.log(values.dateOfBirth);
    this.sharedsevice.insertData('patient information is received , enter patient insurance  details');
  }
  memberIdStore(values) {
    console.log(values.memberId);
  }
  claimRuleCategoryIdStore(values) {
    console.log(values.claimRuleCategoryId);
  }
  claimSubmissionCategoryIdStore(values) {
    console.log(values.claimSubmissionCategoryId);
  }
  insuranceReportingCategoryIdStore(values) {
    console.log(values.insuranceReportingCategoryId);
    this.sharedsevice.insertData('You completed the second step.');
  }
  rpfirstNameStore(values) {
    console.log(values.rpfirstName);
  }
  rplastNameStore(values) {
    console.log(values.rplastName);
  }
  rnpiStore(values) {
    console.log(values.rnpi);
  }
  rBilledNameStore(values) {
    console.log(values.rBilledName);
  }
  rproviderTypeStore(values) {
    console.log(values.rproviderType)
  }
  rMedicalNpiStore(values) {
    console.log(values.rMedicalNpi);
    this.sharedsevice.insertData('Rendering provider information received, Enter Supervising provider information');
  }
  spfirstNameStore(values) {
    console.log(values.spfirstName);
  }
  splastNameStore(values) {
    console.log(values.splastName);
  }
  snpiStore(values) {
    console.log(values.snpi);
  }
  sBilledNameStore(values) {
    console.log(values.sBilledName);
  }
  sproviderTypeStore(values) {
    console.log(values.sproviderType);
  }
  sMedicalNpiStore(values) {
    console.log(values.sMedicalNpi);
    this.sharedsevice.insertData('Third step completed');
  }
  code1Store(values) {
    console.log(values.code1);
  }
  code2Store(values) {
    console.log(values.code2);
  }
  code3Store(values) {
    console.log(values.code3);
  }
  code4Store(values) {
    console.log(values.code4);
  }
  date1Store(values) {
    console.log(values.date1);
  }
  date2Store(values) {
    console.log(values.date2);
  }
  date3Store(values) {
    console.log(values.date3);
  }
  date4Store(values) {
    console.log(values.date4);
  }
  id1Store(values) {
    console.log(values.id1);
  }
  id2Store(values) {
    console.log(values.id2);
  }
  id3Store(values) {
    console.log(values.id3);
  }
  id4Store(values) {
    console.log(values.id4);
  }

  validationOfClaimType(values) {
    if(values.healthCareClaimType > 2) {
      this.healthCareBackground=true;
      this.sharedsevice.insertData('Only helath care types 1,2 are valid. Please check again');
    }
    else {
      this.sharedsevice.insertData('Health care claim type filed passed the validation');
      this.healthCareBackground=false;
    }
  }
  validationOfStatus(values) {

     if(values.status == 'open' || values.status == 'close') {
       this.statusBackground=false;
       this.sharedsevice.insertData('Claim status filed looks good');
     }
     else {
       this.statusBackground=true;
       this.sharedsevice.insertData('Only open and close are allowed for claim status');
     }
  }

  printSuccessMessage() {
    this.successFlag = true;
    alert('claim create successfully');
  }

  //callback function

  myCallBack() {

  }

//apicall

  apicall(values) {
    console.log(values.claimFormat);
    console.log(values.healthCareClaimType);
      this.userservice.test(values).subscribe((data) => {
          console.log(data);
          this.data= data;
          console.log(data.listOfMessages);
        console.log(data.listOfMessages[0]);
        console.log(data.listOfMessages[1]);
          this.sharedsevice.insertData(data.message);
        if(this.data.map != null) {
            this.ocCodesFlag=this.data.map.flag;
            if(this.data.map.flag == "true") {
              this.ocCodesFlag=true;
              this.sharedsevice.insertData(this.data.map.list);
            }
            else  {
              this.ocCodesFlag=false;
            }
        }
    }
      );

  }




  pciapistore(values){
    this.procedurecodeapi ='';
    this.diagnosiscodeapi ='';
    this.procedurecodeapi = values.pci;
    console.log(this.procedurecodeapi);

    this.diagnosiscodeapi = values.dc;
    console.log(  this.diagnosiscodeapi);
    if(this.procedurecodeapi !== ''  &&this.diagnosiscodeapi !== '' ){
      console.log("two are filled");

      this.userservice.procedurecheck(values).subscribe((data) => {
        console.log(data);
    this.data= data;
          if(data.allow == true){

            this.sharedsevice.insertData( 'Looking good, '+ values.pci +'  matched with diagnosis code ' + values.dc);

          }
          else {
            this.sharedsevice.insertData( 'Conflict: '+ values.pci +'  not matched with diagnosis code ' + values.dc);
          }


        }/*,err => {
        this.show=false;
        this.errbutton = true;
        this.errors = err.cause.message;
        console.log(err.cause.message);
      }*/
      );
    }
  }
}
