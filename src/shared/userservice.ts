
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ApiServices } from '../shared/api.service';
@Injectable()
export class UserService {
  constructor(private apiAdvServices: ApiServices,
              private http: Http,
  ) { }

  procedurecheck(params) {
    const param = {
      'code': {
        'id': 3,
        'pCode': params.pci,
        'dCode': params.dc
      },
      '__DecisionID__': 'b859576c-fa34-4562-a847-d801a4e92ec60'
    }
    console.log(params);
    return this.apiAdvServices.post('http://localhost:8001/claimvalidation', JSON.stringify(param), 'raw')
      .map(data => {
        return data;
      });
  }

  public procedurecheking(value) {
    const params: URLSearchParams = new URLSearchParams();
    return this.apiAdvServices.get( 'http://localhost:5214/chatbot/'+value , params , '' )
      .map(data => {
        return data;
      });

  }
  public test(params) {

    const param ={
      "cval": {
        "id": 3
      },
      "__DecisionID__": "string",
      "claim": {
        "attributes": {
          "healthCareClaimTypeId": params.healthCareClaimType,
          "claimFormat": params.claimFormat,
          "status": params.status,
          "contextId": params.contextId,
          "created": params.created,
          "id": 3,
          "description": params.description
        },
        "claimRuleCategories": {
          "claimRuleCategory": {
            "id": params.claimRuleCategory,
            "claimRuleCategories": {
              "claimRuleCategory": params.claimRuleCategory
            }
          }
        },
        "patient": {
          "id": 3,
          "firstName": params.firstName,
          "lastName": params.lastName,
          "dateOfBirth": params.dateOfBirth
        },
        "primaryInsurance": {
          "memberId": 3,
          "claimRuleCategoryId": params.claimRuleCategoryId,
          "claimSubmissionCategoryId": params.claimSubmissionCategoryId,
          "id": 3,
          "insuranceReportingCategoryId": params.insuranceReportingCategoryId
        },
        "renderingProvider": {
          "medicalGroup": {
            "id": 3,
            "npi": params.rMedicalNpi
          },
          "firstName": params.rpfirstName,
          "lastName": params.rplastName,
          "npi": params.rnpi,
          "billedName": params.rBilledName,
          "providerType": params.rproviderType,
          "id": 3
        },
        "supervisingProvider": {
          "firstName": params.spfirstName,
          "providerType": params.splastName,
          "id": "2",
          "medicalGroup": {
            "id": 3,
            "npi": params.sMedicalNpi
          },
          "lastName": params.splastName,
          "npi": params.snpi,
          "billedName": params.sBilledName
        },
        "status": params.status,
        "occurrenceCodes": {
          "occurrenceCode": [
            {
              "date": params.date1,
              "id": params.id1,
              "code": params.code1
            },
            {
              "date": params.date2,
              "id": params.id2,
              "code": params.code2
            },
            {
              "date": params.date3,
              "id": params.id3,
              "code": params.code3
            },
            {
              "date": params.date4,
              "id": params.id4,
              "code": params.code4
            }
          ]
        }
      },
      "message": ""
    };
    console.log("               " + param.claim.attributes.healthCareClaimTypeId);
    return this.apiAdvServices.post('http://localhost:8001/claimvalidation', JSON.stringify(param), 'raw')
      .map(data => {
        return data;
      });
  }
}
